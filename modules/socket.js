const socketio = require('socket.io');

class SocketModule{
    constructor() {
        this.io = socketio(3001);
        this.clients = {client: [], support: [], default: [], all: {}};
        this.dialogs = {};
        this.io.on('connection', socket => {
            socket.on('auth', async payload => {
                this.AppendUser(payload.role, socket);
                this.renderLog();
                socket.on('disconnect', () => {
                    switch(this.clients.all[socket.id].role){
                        case 'client':
                            this.clients.client.splice(this.clients.client.findIndex(user => user.id === socket.id), 1);
                            break;
                        case 'support':
                            this.clients.support.splice(this.clients.support.findIndex(user => user.id === socket.id), 1);
                            break;
                        default:
                            this.clients.default.splice(this.clients.default.findIndex(user => user.id === socket.id), 1);
                    }
                    this.renderLog();
                    delete this.clients.all[socket.id];
                })
            })
        })
    }

    AppendUser(role, socket) {
        const user = new User(socket.id, role);
        switch (role) {
            case 'client':
                if (this.clients.client) {
                    this.clients.client.push(user);
                } else {
                    this.clients.client = [user]
                }
                socket.on('messageSend', async payload => {
                    if (!user.dialogs.length) {
                        user.dialogs = [new Dialog(user)] 
                    }
                    payload.user = user.name;
                    payload.role = user.role;
                    payload.chat = user.id;
                    user.dialogs.forEach(dialog => {
                        dialog.participants.forEach(user => {
                            if (user) {
                                this.io.to(user.id).emit('messageCatch', payload);
                            }
                        })
                    })
                })
                break;
            case 'support':
                if (this.clients.support) {
                    this.clients.support.push(user);
                } else {
                    this.clients.support = [user]
                }
                user.free = true;
                socket.on('messageSend', async payload => {
                    if (!user.dialogs.length) {
                        user.dialogs = [new Dialog(user)] 
                    }
                    payload.user = user.name;
                    payload.role = user.role;
                    payload.chat = payload.source;
                    user.dialogs.forEach(dialog => {
                        if (dialog.id === payload.source) {
                            dialog.participants.forEach(user => {
                                this.io.to(user.id).emit('messageCatch', payload);
                            })
                        }
                    })
                })
                break;
            default:
                if (this.clients.default) {
                    this.clients.default.push(user);
                } else {
                    this.clients.default = [user]
                }
        }
        if (!this.clients.all) {this.clients.all = {}}
        this.clients.all[socket.id] = (user);
        return user;
    }

    renderLog(){
        console.clear();
        console.log('client list: ');
        this.clients.client.forEach(user => {console.log(user.id)});
        console.log('support list: ');
        this.clients.support.forEach(user => {console.log(user.id)});
    }

    Kill(){
        this.io.close();
        delete this;
    }
}

class Dialog{
    constructor(user){
        this.id = user.name;
        this.participants = [user, this.freeSup(user)];
    }

    freeSup(user){
        const freeSup = global.Core.modules.SocketModule.clients.support.reverse().find(user => user.free === true);
        if (freeSup) { freeSup.dialogs.push(this); global.Core.modules.SocketModule.io.to(freeSup.id).emit('newDialog', user) }
        return freeSup;
    }
}

class User{
    constructor(id, role){
        this.id = id;
        this.name = id;
        this.role = role;
        this.dialogs = [];
    }
}

module.exports = SocketModule;
