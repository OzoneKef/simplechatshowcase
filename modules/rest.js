const Express = require('express')();
const path = require('path');

class REST{
    constructor(){
        this.listener = Express.listen(3000);

        Express.use(require('body-parser').urlencoded({ extended: false }));
        Express.use(require('body-parser').json({ type: 'application/json' }));
        Express.use(require('cookie-parser')());

        Express.get('/client', async (request, response) => {
            try {
                response.sendFile(path.resolve('./client/client.html'))
            } catch(error) {
                console.log(error);
            }
        });
        Express.get('/support', async (request, response) => {
            try {
                response.sendFile(path.resolve('./client/support.html'))
            } catch(error) {
                console.log(error);
            }
        });
        Express.use(require('express').static(path.resolve('./client/')));
    }

    Kill(){
        this.listener.close();
        return delete this;
    }
}

module.exports = REST;