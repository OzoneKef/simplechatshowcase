window.onload = () => {
    socketio = io(`${location.hostname}:3001`);
    socketio.on('connect', socket => {
        socketio.emit('auth', {role: 'client'});
        jQuery('.chatWindow > .sendButton').on('click', () => {
            if (jQuery('.chatWindow > input').val() !== '') {
                socketio.emit('messageSend', { message: jQuery('.chatWindow > input').val(), time: new Date() })
                jQuery('.chatWindow > input').val('')
            }
        })
        socketio.on('messageCatch', payload => {
            new Audio('/msg.ogg').play();
            jQuery('.chatWindow > .chatContent').append(`<p><span class='role'>${payload.role} </span><span class='name'>${payload.user} </span><span class='time'>${payload.time}: </span><span class='msg'>${payload.message}</span></p>`);
        })
    })
    
}