window.onload = () => {
    socketio = io(`${location.hostname}:3001`);
    socketio.on('connect', socket => {
        socketio.emit('auth', {role: 'support'});
        socketio.on('messageCatch', payload => {
            new Audio('/msg.ogg').play();
            jQuery(`#chat${payload.chat} > .chatContent`).append(`<p><span class='role'>${payload.role} </span><span class='name'>${payload.user} </span><span class='time'>${payload.time}: </span><span class='msg'>${payload.message}</span></p>`);
        })
        socketio.on('newDialog', payload => {
            new Audio('/new.ogg').play();
            jQuery('.container').append(
                `<div class='chatWindow' id='chat${payload.name}'>
                    <div class='chatContent'></div>
                    <input type='text'>
                    <div class='sendButton'>Отправить</div>
                </div>`
            )
            jQuery(`#chat${payload.name} > .sendButton`).on('click', () => {
                if (jQuery(`#chat${payload.name} > input`).val() !== ''){
                    socketio.emit('messageSend', { message: jQuery(`#chat${payload.name} > input`).val(), time: new Date(), source: payload.name })
                    jQuery(`#chat${payload.name} > input`).val('')
                }
            })
        })
    })
    
}